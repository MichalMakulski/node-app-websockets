var WebSocketServer = require('websocket').server;

module.exports = (httpServer) => new WebSocketServer({ httpServer: httpServer });
