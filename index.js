var https = require('https');
var fs = require('fs');
var serverHandler = require('./server-handler');
var wsServer = require('./ws-server');

var clients = {};
var chat = [];
var port = process.env.PORT || 1337;

var options = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
};

var server = https.createServer(options, serverHandler(clients));

server.listen(port, () => { console.log('Server listening on port ' + port) });

wsServer(server).on('request', function(req) {
   
    var user = req.resourceURL.query.user;
    var connection = req.accept('echo-protocol', req.origin);
    
    clients[user] = {
        connection: connection
    };
        
    console.log((new Date()) + ' Connection accepted [' + user + ']');
    
    connection.on('message', function(msg) {
        
        var msgData = JSON.parse(msg.utf8Data);
        
        if (msgData.type === 'NEW_USER') {
            for (let userId in clients) {
                clients[userId].connection.sendUTF(JSON.stringify({
                  type: 'UPDATE_USERS',
                  users: JSON.stringify(Object.keys(clients)),
                  chatMsgs: JSON.stringify(chat)
                }));
            }
            if (chat.length) {
              clients[user].connection.sendUTF(JSON.stringify({
                type: 'UPDATE_CHAT',
                chatMsgs: JSON.stringify(chat)
              }));
            }
            return;
        }
      
        if (msgData.type === 'CHAT_MSG') {
            chat.push(msgData);
            for (user in clients) {
                clients[user].connection.sendUTF(JSON.stringify(msgData));
            }  
        }
        
        
    });
    
    connection.on('close', function(reasonCode, description) {
      
        delete clients[user];
      
        for (var userName in clients) {
            clients[userName].connection.sendUTF(JSON.stringify({
              type: 'UPDATE_USERS',
              users: JSON.stringify(Object.keys(clients))
            }));
        }
        
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
        
    });
    
});