var fs = require('fs');

module.exports = (clients) => {
  return function (req, res) {
  
      var url = req.url;

      if (url === '/') {
          res.writeHead(200, {'content-type': 'text/html'});
          fs.createReadStream('index.html').pipe(res);
          return;
      }

      if (url.indexOf('public') !== -1) {
          const fileName = url.replace(/\/public\/(\w)/, '$1');
          fs.createReadStream(`${__dirname}/client/${fileName}`, 'utf8').pipe(res);
          return;
      }

      if (url === '/set-new-user') {
          var user = '';
          req.on('data', (chunk) => {
              user += chunk.toString();  
          });
          req.on('end', () => {
              console.log('NEW USER: ', user);
              res.writeHead(200, {'content-type': 'text-plain'});

              if (user in clients) {
                  res.end('User already exists');  
              }
              else {
                  res.end('Thank you for joining');
              }
          });      
      }
  }
}