;(function(){
    var ws;   
        
    setNewUser('Your nickname:');

    function setNewUser (message) {
        const userName = window.prompt(message);

        if (userName === null) {
            document.body.innerHTML('<h1>Some other time maybe</h1>');
            return;  
        }

        if (userName === '') {
            setNewUser('Your nickname:');
        }
        const headers = new Headers({'Content-Type': 'text/plain'});
        const reqSetup = {
            method: 'POST',
            headers: headers,
            body: userName
        }
        const req = new Request('/set-new-user', reqSetup);

        fetch(req)
          .then((resp) => resp.text())
          .then((respTxt) => {
              if (respTxt === 'User already exists') {
                  setNewUser(respTxt);  
              } else {
                  askServerForHandshake(userName, bindEvents);
              }
          });
    }

    function askServerForHandshake (user, callback) {
        var host = window.location.host;
        ws = new WebSocket('ws://' + host + '?user=' + user, 'echo-protocol');
        callback(user);
    }
  
    function bindEvents (user) {
        document.querySelector('.msg-container button').addEventListener('click', function() {
            sendMessage(user, 'CHAT_MSG');
        }, false);

        ws.addEventListener("open", function(ev) {
            sendMessage(user, 'NEW_USER');
        });

        ws.addEventListener("message", function(ev) {
            var msgData = JSON.parse(ev.data);

            if (msgData.type === 'UPDATE_USERS') {              
                let usersHTML =  JSON.parse(msgData.users).map(function(user) {
                    return '<li>' + user +'</li>';
                }).join('');
                
                document.querySelector('.users').innerHTML = usersHTML;
                
                return;
            }
          
            if (msgData.type === 'UPDATE_CHAT') {
                let historicChatHTML = JSON.parse(msgData.chatMsgs).map(function(msg) {
                        return '<li><span>' + msg.user + '</span> ' + msg.msg + '</li>';
                    }).join('');
                document.querySelector('.historic-chat').innerHTML = historicChatHTML;
                return;
            }

            document.querySelector('.chat').insertAdjacentHTML('beforeend', 
            '<li><span>' + msgData.user + '</span> ' + msgData.msg + '</li>'                                                  
            );
        });
    }

    function sendMessage (user, msgType) {
        var data = {
            type: msgType,
            user: user,
            msg: document.querySelector('.msg').value
        }

        ws.send(JSON.stringify(data));
    }
  
})();